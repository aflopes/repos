# README #

Tool to manage several SVN repositories 


## How to install ##
1. Checkout this repository
2. Execute: chmod +x install.sh
3. Run the install.sh script as root

## How to use ##
1. Create a file named .repos with the local paths and repositories 
2. Execute *repos <option>*

## .repos file format ##
<local path> <repository url>
This file can have empty lines
The lines starting with an # are ignored (comments)